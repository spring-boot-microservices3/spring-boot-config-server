## SPRING BOOT CONFIG SERVER

# requirement ENV
- docker postgresql / your local postgresql
- java 11

```
├── README.md
├── Dockerfile
├── pom.xml
└── src
    ├── main
    │   ├── java
    │   │   └── spring.boot.configserver
    │   ├── resources
    │   │   └── application.properties
```
change postgresql port and host on `application.properties`

and run the services
```aspectj
mvn springb-boot:run
```


